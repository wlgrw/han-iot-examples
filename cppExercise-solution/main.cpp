/*
 * This file and its content is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; Without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/*!
 * \file main.cpp
 * \author Remko Welling (remko.welling@han.nl)
 * \version 1
 * \brief Introduction C++ programming exercise 1
 * \copyright Creative Commons Attribution-NonCommercial 4.0 International License (http://creativecommons.org/licenses/by-nc/4.0/) by Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl.
 *
 * # Excercise 1
 * Study the example code and implement the code according to the assignment.
 *
 * ## Assignment 1 & 2
 * Implement the print to screen activity using C++ ctyle (cout).
 *
 * With these assignments a C-style struct is used to hold 4 types of information.
 * The information is printed using printf() and the printf() statement shall be
 * replaced using C++ -style cout statements.
 *
 * ### Question:
 * What is the purpose of "endl" in the printing command?
 *
 * ## Assignment 3 & 4
 * Implement the print to screen activity using C++ ctyle (cout).
 *
 * With these assignments a C++ class is used to hold 4 types of information.
 * The information is printed using printf() and the printf() statement shall be
 * replaced using C++ -style cout statements.
 *
 * Some variables in the class are not "public" but "private" and therefore cannot
 * be printed.
 *
 * ### Question:
 * Which variables cannot be printed?
 *
 * ## Assignment 5 & 6
 * Implement the print to screen activity using C++ ctyle (cout).
 *
 * With these assignments a C++ class is used to hold 4 types of information.
 * The information is printed using printf() and the printf() statement shall be
 * replaced using C++ -style cout statements.
 *
 * Some variables in the class are not "public" but "private" and therefore cannot
 * be accessed directly. To ovecome this issue 2 functions shall be added to the
 * class. These functions shall be "public" as they have to be accessible from
 * "outside" the class.
 *
 * Functions to access private variables in a class are called "setters" and "getters"
 *
 * ## How to use the file?
 * The file is split in to 3 exercises using #defines. #defines are handled by the
 * pre-compiler and will in- and exclude parts of the code for the compiler.
 *
 * To select an exercise uncomment the associated #define.
 *
 */

#include <iostream>
#include <string.h> // strcpy

//#define EXERCISE_1
//#define EXERCISE_2
#define EXERCISE_3


using namespace std;

#if defined(EXERCISE_1)
/****************************************************************************
 * START EXERCISE 1
 ****************************************************************************/

struct Books {          ///< struct with type-name "Books
    char  title[50];     ///< Character array of 50 characters (string)
    char  author[50];    ///< Character array of 50 characters (string)
    char  subject[100];  ///< Character array of 100 characters (string)
    int   book_id;       ///< integer variable
};

int main()
{
    // cout <<, is used to print anything on screen, same as printf in C language.
    // cin and cout are same as scanf and printf, only difference is that you do
    // not need to mention format specifiers like, %d for int etc, in cout & cin.

    cout << "Exercise 1:" << endl;

    // Declare Book1 of type Books
    // Warning! this struct is unitialised.
    // Question: How to print the content of an unitialised struct to the screen?
    struct Books Book1;

    // book 1 specification
    strcpy( Book1.title, "C Programming");
    strcpy( Book1.author, "Nuha Ali");
    strcpy( Book1.subject, "C Programming Tutorial");
    Book1.book_id = 6495407;

    cout << "Print Book1 info \"c-style\"" << endl;

    printf( "Book 1 title : %s\n", Book1.title);
    printf( "Book 1 book_id : %d\n", Book1.book_id);




    // Assignment 1: Execute the following task:
    cout << "Assignment 1: print the book title \"c++ style\" using cout" << endl;

    // Answer 1:
    cout << "Book 1 title : " << Book1.title << endl; // answer 1




    // Assignment 2: Execute the following task:
    cout << "Assignment 2: print the book-id \"c++ style\" using cout" << endl;

    // Answer 2:
    cout << "Book 1 book_id : " << Book1.book_id  << endl; // answer 2

    return 0;
}

#elif defined(EXERCISE_2)

/****************************************************************************
 * START EXERCISE 2
 ****************************************************************************/

class Books		// <keyword> <user-defined-name>
{
private:						// Access specifier: private|public|protected
    char  author[50];    ///< Character array of 50 characters (string)
    char  subject[100];  ///< Character array of 100 characters (string)

public:						// Access specifier: private|public|protected
    char  title[50];     ///< Character array of 50 characters (string)
    int   book_id;       ///< integer variable
};

int main()
{
    cout << "Exercise 2:" << endl;

    // Instantiate Book1 of class Books
    Books Book1;

    // book 1 specification
    strcpy( Book1.title, "C Programming");
    strcpy( Book1.author, "Nuha Ali");
    strcpy( Book1.subject, "C Programming Tutorial");
    Book1.book_id = 6495407;

    cout << "Print Book1 info \"c-style\"" << endl;

    printf( "Book 1 title : %s\n", Book1.title);
    printf( "Book 1 book_id : %d\n", Book1.book_id);



    // Assignment 3: Execute the following task:
    cout << "Assignment 3: print the book title \"c++ style\" using cout" << endl;

    // answer 3
    cout << "Book 1 title : " << Book1.title << endl;




    // Assignment 4: Execute the following task:
    cout << "Assignment 4: print the book-id \"c++ style\" using cout" << endl;

    // answer 4
    cout << "Book 1 book_id : " << Book1.book_id  << endl;

    return 0;
}

#elif defined(EXERCISE_3)
/****************************************************************************
 * START EXERCISE 3
 ****************************************************************************/

class Books		// <keyword> <user-defined-name>
{
private:			     // Access specifier: private|public|protected
    char  author[50];    ///< Character array of 50 characters (string)
    char  subject[100];  ///< Character array of 100 characters (string)

public:				// Access specifier: private|public|protected
    char  title[50];     ///< Character array of 50 characters (string)
    int   book_id;       ///< integer variable

    void setAuthor(const char *buffer);
    char *getAuthor(){return author;};
};

void Books::setAuthor(const char *buffer){
    strcpy(author, buffer);
}

int main()
{
    cout << "Exercise 3:" << endl;

    // Instantiate Book1 of class Books
    Books Book1;
    // book 1 specification
    strcpy(Book1.title, "C Programming");
    // book 1 ID
    Book1.book_id = 6495407;

    // Assignment 5: Execute the following task:
    cout << "Assignment 5: Set the author of the book using a \"setter\" in the class." << endl;
    // replace the folloing c-style code by a setter with the same value.
//    strcpy( Book1.author, "Nuha Ali");

    // Answer 5
    Book1.setAuthor("Nuha Ali");



    cout << "Book 1 title : " << Book1.title << endl;




    // Assignment 6: Execute the following task:
    cout << "Assignment 6: Print the book-author \"c++ style\" using cout and a \getter\" in the class." << endl;

    // answer 6
    cout << "Book 1 author : " << Book1.getAuthor() << endl;




    cout << "Book 1 book_id : " << Book1.book_id  << endl;

    return 0;
}

#endif
