# Simple program to explain encryptiopn
C program to encrypt and decrypt the string using Caesar Cypher Algorithm.

For encryption and decryption, we have used 3 as a key value.

While encrypting the given string, 3 is added to the ASCII value of the characters. Similarly, for decrypting the string, 3 is subtracted from the ASCII value of the characters to print an original string.

# Compile
```
gcc -Wall main.c -o simpleEncryption
```

## Disclaimer
The code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; Without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

## License
This code wat taken from http://www.trytoprogram.com/c-examples/c-program-to-encrypt-and-decrypt-string/ and adapted by Remko Welling.

